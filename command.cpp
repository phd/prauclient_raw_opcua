#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <syslog.h>
#include <QDebug>
#include <QDir>
#include <sys/ioctl.h>
#include <net/if.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "main.h"


DWORD OPCUACommand::invokeid = 5;
DWORD OPCUACommand::security_token_id=1;
DWORD OPCUACommand::security_seq_number=52;
DWORD OPCUACommand::security_req_id=2;

int OPCUACommand::SecureMsg(QString str)
{
    mylog(5,"OPCUACommand::SecureMsg...");
    //mylog(5,"ip1=%02X",ip1);    mylog(5,"ip2=%02X",ip2);    mylog(5,"ip3=%02X",ip3);    mylog(5,"ip4=%02X",ip4);

    BYTE* debut = _internal_buf;// + 2;
    BYTE* ptr = debut;
    
    *ptr++ = 'M'; // H
    *ptr++ = 'S'; // E
    *ptr++ = 'G'; // L
    *ptr++ = 'F'; // F

    // message size 4 BYTES    
    BYTE* ptr_len1 = ptr; 
    *ptr = 0x11; ptr++ ;
    *ptr = 0x22; ptr++ ;
    *ptr = 0x33; ptr++ ;
    *ptr = 0x44; ptr++ ;


    _internal_buf_size = ptr - debut;
    mylog(2,"OPCUACommand::SecureMsg _internal_buf_size=%d...",_internal_buf_size);
    //*(DWORD*)ptr_len1 = (ptr-debut);
        *ptr_len1 = (int)_internal_buf_size%256; ptr_len1++ ;
        *ptr_len1 = (int)_internal_buf_size/256; ptr_len1++ ;
        *ptr_len1 = (int)_internal_buf_size/(256*256); ptr_len1++ ;
        *ptr_len1 = (int)_internal_buf_size/(256*256*256); ptr_len1++ ;    
    return _internal_buf_size ;
}

int OPCUACommand::GetEndPointRequest
    (DWORD channel_id, DWORD security_id, DWORD security_seq_number, 
    DWORD security_req_id, QString endpoint_url)
{
    mylog(2,"--> SecureMsgGetEndPointRequest %d...",channel_id);
    //mylog(5,"ip1=%02X",ip1);    mylog(5,"ip2=%02X",ip2);    mylog(5,"ip3=%02X",ip3);    mylog(5,"ip4=%02X",ip4);

    BYTE* debut = _internal_buf;// + 2;
    BYTE* ptr = _internal_buf;
    
    *ptr++ = 'M'; // H
    *ptr++ = 'S'; // E
    *ptr++ = 'G'; // L
    *ptr++ = 'F'; // F

    // message size 4 BYTES    
    BYTE* ptr_len1 = ptr; 
    *ptr = 0x11; ptr++ ;
    *ptr = 0x22; ptr++ ;
    *ptr = 0x33; ptr++ ;
    *ptr = 0x44; ptr++ ;

    //hexDump();

    // secure_channel_id 4 BYTES    0x22 0x1f 0x00 0x00
    //*(DWORD*)ptr = (DWORD)channel_id; 
    mylog(2,"--> SecureMsgGetEndPointRequest %d",channel_id);
        BYTE* ptr_tmp = ptr;
                *ptr = channel_id % 256; ptr++ ;
                *ptr = channel_id / 256; ptr++ ;
                *ptr = channel_id / 256*256; ptr++ ;
                *ptr = channel_id / 256*256*256; ptr++ ;
    mylog(2,"--> SecureMsgGetEndPointRequest %02X %02X %02X %02X",ptr_tmp[0],ptr_tmp[1],ptr_tmp[2],ptr_tmp[3]);
    //::hexdump(ptr,4);
    //ptr++ ;ptr++ ;ptr++ ;ptr++ ;
    //hexDump();


    // security_token_id 4 BYTES    0x22 0x1f 0x00 0x00
    *(DWORD*)ptr = security_token_id; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

    // security_seq_number 4 BYTES    0x22 0x1f 0x00 0x00
    *(DWORD*)ptr = security_seq_number; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

    // security_req_id  4 BYTES    0x22 0x1f 0x00 0x00
    *(DWORD*)ptr = security_req_id ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

    security_seq_number++;
    security_req_id++;



    /// ---> OPCUA SERVICE
        // type_id: expandedNodeId
            // nodeid encoding mask
            *ptr = 1; ptr++ ; // 4 bytes encoded numeric
            // nodeid namespace index
            *ptr = 0; ptr++ ;
            // getendpointrequest: 428
            *(WORD*)ptr = OpcUaMessageId::GET_ENDPOINTS_REQUEST ;ptr++ ;ptr++ ; // 0xAC 0x01


        /// ---> GET_ENDPOINT_REQUEST
            // authentication token: nodeid
            *ptr = 0; ptr++ ; // encoding mask: 2 bytes encoded numeric
            *ptr = 0; ptr++ ; // identifier numeric

            // timestamp 8 bytes                
            *ptr = 0x80; ptr++ ;
            *ptr = 0xeb; ptr++ ;
            *ptr = 0xad; ptr++ ;
            *ptr = 0xe6; ptr++ ;                
            *ptr = 0x15; ptr++ ;
            *ptr = 0xc1; ptr++ ;
            *ptr = 0xd6; ptr++ ;
            *ptr = 0x01; ptr++ ;

            // request handle 4 BYTES   
            *(DWORD*)ptr =2; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

            // return diag 4 BYTES   
            *(DWORD*)ptr =0; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

            // AuditEntryId STRING NULL 4 BYTES  0xffffffff
            *(DWORD*)ptr =0xffffffff; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

            // timeout int  4 BYTES   
            *(DWORD*)ptr =1000; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

            // additionnal header 3 BYTES   : extension object
                // expandedNodeId mask
                    // type id : expanded node
                    *(BYTE*)ptr =0; ptr++ ; // encoding mask : 2 bytes encoded numeric
                    // identifier numeric
                    *(BYTE*)ptr =0; ptr++ ;
                // encoded mask : 0
                *(BYTE*)ptr =0; ptr++ ;


            // endpoint url
                // length
                //*(DWORD*)ptr = endpoint_url.length();  ptr++ ;ptr++ ;ptr++ ;ptr++ ;
                *ptr = (int)endpoint_url.length()%256; ptr++ ;
                *ptr = (int)endpoint_url.length()/256; ptr++ ;
                *ptr = (int)endpoint_url.length()/(256*256); ptr++ ;
                *ptr = (int)endpoint_url.length()/(256*256*256); ptr++ ;
                
                // string data
                for (int i=0; i<endpoint_url.length() ; i++){
                    *ptr++ = endpoint_url[i].toAscii();
                    //*ptr++ = '1'; *ptr++ = '9';*ptr++ = '3';*ptr++ = '0' ;  // ascii string
                    //*ptr++ = digit1; *ptr++ = digit2;*ptr++ = digit3;*ptr++ = digit4;  // ascii string
                }   

            // LocaleIds  4 BYTES   
            *(DWORD*)ptr =0; ptr++ ;ptr++ ;ptr++ ;ptr++ ;
            // ProfileURIs  4 BYTES   
            *(DWORD*)ptr =0; ptr++ ;ptr++ ;ptr++ ;ptr++ ;
            
        /// ---< GET_ENDPOINT_REQUEST
    /// ---< OPCUA SERVICE

    _internal_buf_size = ptr - debut;
    mylog(2,"OPCUACommand::SecureMsgGetEndPointRequest _internal_buf_size=%d...",_internal_buf_size);
    //*(DWORD*)ptr_len1 = (ptr-debut);
        *ptr_len1 = (int)_internal_buf_size%256; ptr_len1++ ;
        *ptr_len1 = (int)_internal_buf_size/256; ptr_len1++ ;
        *ptr_len1 = (int)_internal_buf_size/(256*256); ptr_len1++ ;
        *ptr_len1 = (int)_internal_buf_size/(256*256*256); ptr_len1++ ;    
    return _internal_buf_size ;
}

int OPCUACommand::OpenSecureChannel(QString policy)
{
    mylog(2,"OPCUACommand::OpenSecureChannel...");
    //mylog(5,"ip1=%02X",ip1);    mylog(5,"ip2=%02X",ip2);    mylog(5,"ip3=%02X",ip3);    mylog(5,"ip4=%02X",ip4);

    BYTE* debut = _internal_buf;// + 2;
    BYTE* ptr = _internal_buf;
    
    *ptr++ = 'O'; // H
    *ptr++ = 'P'; // E
    *ptr++ = 'N'; // L
    *ptr++ = 'F'; // F

    // message size 4 BYTES    
    BYTE* ptr_len1 = ptr; 
    *ptr = 0x11; ptr++ ;
    *ptr = 0x22; ptr++ ;
    *ptr = 0x33; ptr++ ;
    *ptr = 0x44; ptr++ ;

    // SecureChannelId 4 BYTES
    *ptr = 0; ptr++ ;
    *ptr = 0; ptr++ ;
    *ptr = 0; ptr++ ;
    *ptr = 0; ptr++ ;

    // SecurityPolicyUri
        // length
        //*(DWORD*)ptr = endpoint_url.length();  ptr++ ;ptr++ ;ptr++ ;ptr++ ;
        *ptr = (int)policy.length()%256; ptr++ ;
        *ptr = (int)policy.length()/256; ptr++ ;
        *ptr = (int)policy.length()/(256*256); ptr++ ;
        *ptr = (int)policy.length()/(256*256*256); ptr++ ;
        
        // string data
        for (int i=0; i<policy.length() ; i++){
            *ptr++ = policy[i].toAscii();
            //*ptr++ = '1'; *ptr++ = '9';*ptr++ = '3';*ptr++ = '0' ;  // ascii string
            //*ptr++ = digit1; *ptr++ = digit2;*ptr++ = digit3;*ptr++ = digit4;  // ascii string
        }        

    // SenderCertificate 4 BYTES
    *ptr = 0xff; ptr++ ;
    *ptr = 0xff; ptr++ ;
    *ptr = 0xff; ptr++ ;
    *ptr = 0xff; ptr++ ;

    // ReceiverCertificateThumbPrint 4 BYTES
    *ptr = 0xff; ptr++ ;
    *ptr = 0xff; ptr++ ;
    *ptr = 0xff; ptr++ ;
    *ptr = 0xff; ptr++ ;

    // SequenceNumber 4 BYTES
    *ptr = 1; ptr++ ;
    *ptr = 0; ptr++ ;
    *ptr = 0; ptr++ ;
    *ptr = 0; ptr++ ;

    // RequestId 4 BYTES
    *ptr = 1; ptr++ ;
    *ptr = 0; ptr++ ;
    *ptr = 0; ptr++ ;
    *ptr = 0; ptr++ ;
    
    // MESSAGE
        // TypeId:
            // NodeId encoding mask
            *ptr = 0x01; ptr++ ; // four byte encoded numeric = 0x01
            // NodeId namespace index
            *ptr = 0x00; ptr++ ; // index 0
            // NodeId identifier numeric
            *ptr = (int)446%256; ptr++ ; // OpenSecureChannelRequest=446 0xBE
            *ptr = (int)446/256; ptr++ ; // OpenSecureChannelRequest=446 0x01
        // request:            
            // request header:            
            // request 
                // Athentication token 2 BYTES
                *ptr = 0; ptr++ ; // 0000 = mask = 2 bytes encoded numeric
                *ptr = 0; ptr++ ; // 0 identifier numeric

                // timestamp 8 BYTES
                *ptr = 0x80; ptr++ ;
                *ptr = 0xeb; ptr++ ;
                *ptr = 0xad; ptr++ ;
                *ptr = 0xe6; ptr++ ;                
                *ptr = 0x15; ptr++ ;
                *ptr = 0xc1; ptr++ ;
                *ptr = 0xd6; ptr++ ;
                *ptr = 0x01; ptr++ ;

                // RequestHandle 4 BYTES
                *ptr = 1; ptr++ ;
                *ptr = 0; ptr++ ;
                *ptr = 0; ptr++ ;
                *ptr = 0; ptr++ ;

                // Return Diagnostics size 4 BYTES
                *ptr = 0; ptr++ ;
                *ptr = 0; ptr++ ;
                *ptr = 0; ptr++ ;
                *ptr = 0; ptr++ ;   

                // AuditEntryId 4 BYTES
                *ptr = 0xff; ptr++ ;
                *ptr = 0xff; ptr++ ;
                *ptr = 0xff; ptr++ ;
                *ptr = 0xff; ptr++ ;

                // TimeoutHint 4 BYTES
                *ptr = 0x60; ptr++ ;
                *ptr = 0xea; ptr++ ;
                *ptr = 0; ptr++ ;
                *ptr = 0; ptr++ ;

                // Additional Header extension object
                *ptr = 0x0; ptr++ ; // typeId
                *ptr = 0x0; ptr++ ; // 
                *ptr = 0x0; ptr++ ; // EncodingMask


            // ClientProtocol version size 4 BYTES
            *ptr = 0; ptr++ ;
            *ptr = 0; ptr++ ;
            *ptr = 0; ptr++ ;
            *ptr = 0; ptr++ ;        

            // SecurityTokenRequest size 4 BYTES
            *ptr = 0; ptr++ ;
            *ptr = 0; ptr++ ;
            *ptr = 0; ptr++ ;
            *ptr = 0; ptr++ ;        

            // MessageSecurityMode size 4 BYTES
            *ptr = 0x01; ptr++ ;
            *ptr = 0; ptr++ ;
            *ptr = 0; ptr++ ;
            *ptr = 0; ptr++ ;        

            // ClientNonce 4 BYTES
            *ptr = 0xff; ptr++ ;
            *ptr = 0xff; ptr++ ;
            *ptr = 0xff; ptr++ ;
            *ptr = 0xff; ptr++ ;

            // RequestLifetime size 4 BYTES = 60000
            *ptr = 0xc0; ptr++ ;
            *ptr = 0x27; ptr++ ;
            *ptr = 0x09; ptr++ ;
            *ptr = 0; ptr++ ;               

    _internal_buf_size = ptr - debut;
    mylog(2,"OPCUACommand::OpenSecureChannel _internal_buf_size=%d...",_internal_buf_size);
    //ptr--;*ptr_len1 = (ptr-ptr_len1);
    //*(DWORD*)ptr_len1 = (ptr-debut);
        *ptr_len1 = (int)_internal_buf_size%256; ptr_len1++ ;
        *ptr_len1 = (int)_internal_buf_size/256; ptr_len1++ ;
        *ptr_len1 = (int)_internal_buf_size/(256*256); ptr_len1++ ;
        *ptr_len1 = (int)_internal_buf_size/(256*256*256); ptr_len1++ ;    
    return _internal_buf_size ;
}
