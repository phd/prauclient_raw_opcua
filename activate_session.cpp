#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <syslog.h>
#include <QDebug>
#include <QDir>
#include <sys/ioctl.h>
#include <net/if.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "main.h"

int OPCUACommand::ActivateSessionRequest()
{
    mylog(5,"OPCUACommand::ActivateSessionRequest...");
    //mylog(5,"ip1=%02X",ip1);    mylog(5,"ip2=%02X",ip2);    mylog(5,"ip3=%02X",ip3);    mylog(5,"ip4=%02X",ip4);

    BYTE* debut = _internal_buf;// + 2;
    BYTE* ptr = debut;
    
    *ptr++ = 'M'; // H
    *ptr++ = 'S'; // E
    *ptr++ = 'G'; // L
    *ptr++ = 'F'; // F

    // message size 4 BYTES    
    BYTE* ptr_len1 = ptr; 
    *ptr = 0x11; ptr++ ;
    *ptr = 0x22; ptr++ ;
    *ptr = 0x33; ptr++ ;
    *ptr = 0x44; ptr++ ;




   mylog(2, "--> ActivateSessionRequest _SecureChannelId= %d %08X", myOPCUAlink._SecureChannelId, myOPCUAlink._SecureChannelId);
    BYTE *ptr_tmp = ptr;
    *ptr = myOPCUAlink._SecureChannelId % 256;
    ptr++;
    *ptr = myOPCUAlink._SecureChannelId / (256);
    ptr++;
    *ptr = myOPCUAlink._SecureChannelId / (256 * 256);
    ptr++;
    *ptr = myOPCUAlink._SecureChannelId / (256 * 256 * 256);
    ptr++;
    mylog(2, "--> ActivateSessionRequest _SecureChannelId= %02X %02X %02X %02X", ptr_tmp[0], ptr_tmp[1], ptr_tmp[2], ptr_tmp[3]);

    // security_token_id 4 BYTES    0x22 0x1f 0x00 0x00
    *(DWORD *)ptr = myOPCUAlink._SecurityTokenId;
    ptr++;
    ptr++;
    ptr++;
    ptr++;

    // security_seq_number 4 BYTES    0x22 0x1f 0x00 0x00
    *(DWORD *)ptr = myOPCUAlink._SecuritySequenceNumber;
    ptr++;
    ptr++;
    ptr++;
    ptr++;

    // security_req_id  4 BYTES    0x22 0x1f 0x00 0x00
    *(DWORD *)ptr = myOPCUAlink._SecurityRequestId;
    ptr++;
    ptr++;
    ptr++;
    ptr++;

    myOPCUAlink._SecuritySequenceNumber++;
    myOPCUAlink._SecurityRequestId++;

    /// ---> OPCUA SERVICE
        // type_id: expandedNodeId
            // nodeid encoding mask
            *ptr = 1; ptr++ ; // 4 bytes encoded numeric
            // nodeid namespace index
            *ptr = 0; ptr++ ;
            // ACTIVATE_SESSION_REQUEST: 467
            *(WORD*)ptr = OpcUaMessageId::ACTIVATE_SESSION_REQUEST ;ptr++ ;ptr++ ; // 0xd3 0x01    
        // read request
            // read request header
                // authenticationToken: nodeid
                *ptr = 0x04; ptr++ ; // encoding mask GUID 0x04
                *ptr = 0x01; ptr++ ; // namespace index
                *ptr = 0x00; ptr++ ; // namespace index
                
                // GUID
                for (int k=0;k<16;k++) { 
                    
                    *(BYTE*)ptr = myOPCUAlink._authTokenGUID[k]; 
                    mylog(2, "--> ActivateSessionRequest _authTokenGUID= %02X %02X ",*(BYTE*)ptr, myOPCUAlink._authTokenGUID[k] );
                    ptr++;                    
                }
                /**ptr = 0xd6; ptr++ ; // GUID 16 bytes
                *ptr = 0xc0; ptr++ ; // GUID
                *ptr = 0x77; ptr++ ; // GUID
                *ptr = 0xfe; ptr++ ; // GUID
                *ptr = 0x13; ptr++ ; // GUID
                *ptr = 0xf9; ptr++ ; // GUID
                *ptr = 0x26; ptr++ ; // GUID
                *ptr = 0x42; ptr++ ; // GUID
                *ptr = 0x81; ptr++ ; // GUID
                *ptr = 0x20; ptr++ ; // GUID
                *ptr = 0x04; ptr++ ; // GUID
                *ptr = 0x8b; ptr++ ; // GUID
                *ptr = 0x65; ptr++ ; // GUID
                *ptr = 0x8a; ptr++ ; // GUID
                *ptr = 0x6b; ptr++ ; // GUID
                *ptr = 0xee; ptr++ ; // GUID*/

            // timestamp 8 bytes                
            *ptr = 0x80; ptr++ ;
            *ptr = 0xeb; ptr++ ;
            *ptr = 0xad; ptr++ ;
            *ptr = 0xe6; ptr++ ;                
            *ptr = 0x15; ptr++ ;
            *ptr = 0xc1; ptr++ ;
            *ptr = 0xd6; ptr++ ;
            *ptr = 0x01; ptr++ ;                

                // request handle 4 BYTES    0x22 0x1f 0x00 0x00
                *(DWORD*)ptr = ++myOPCUAlink._requestHandle ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

                // resturn diagnostic 4 BYTES
                *(DWORD*)ptr = 0x00 ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

                // audit entry id 4 BYTES
                *(DWORD*)ptr = 0xffffffff ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

                // timeoutHint 4 BYTES
                *(DWORD*)ptr = 10000; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

            // additional header: extension object
            *ptr = 0x00; ptr++ ;  // typeid expanded node id
            *ptr = 0x00; ptr++ ;
            *ptr = 0x00; ptr++ ; // encoding mask               

            // client signature
                // algo
                *(DWORD*)ptr = 0xffffffff ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;
                // signature : opcua null string
                *(DWORD*)ptr = 0xffffffff ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;
            // clientsoftwarecertificates
                // arraysize: 0
                *(DWORD*)ptr = 0 ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;
            /// localesID "en-US"
                // arraysize:1
                *(DWORD*)ptr = 1 ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;
                // [0] "en-US" length
                *(DWORD*)ptr = 5 ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;
                // [0] "en-US"
                *(BYTE*)ptr = 'e';ptr++; *(BYTE*)ptr = 'n';ptr++; *(BYTE*)ptr = '-';ptr++; *(BYTE*)ptr = 'U';ptr++; *(BYTE*)ptr = 'S';ptr++; 
            // useridentititytoken
                // typeid: expanded node id
                *(BYTE*)ptr =1 ; ptr++; // encoding mask : 4 BYTES encoding
                // namespace: 0
                *(BYTE*)ptr =0 ; ptr++; // namespace 0
                // id numeric: 321 0x41 0x01
                *(WORD*)ptr = 321 ; ptr++; ptr++;
                // encoding mask : 
                *(BYTE*)ptr =1 ; ptr++; // has binary body 0x01
                
                // anonymous identity token
                // anonymous identity token length
                QString anonymous_id_token = QString("OpenOpcUaAnonymousPolicyId"); //OpenOpcUaAnonymousPolicyId
                    *ptr = anonymous_id_token.length()+4;
                    ptr++;
                    *ptr = 0;
                    ptr++;
                    *ptr = 0;
                    ptr++;
                    *ptr = 0;
                    ptr++;                
                    
                    *ptr = anonymous_id_token.length();
                    ptr++;
                    *ptr = 0;
                    ptr++;
                    *ptr = 0;
                    ptr++;
                    *ptr = 0;
                    ptr++;
                // "OpenOpcUaAnonymousPolicyId
                    for (int i = 0; i < anonymous_id_token.length(); i++)    {*ptr++ = anonymous_id_token[i].toAscii();}

            // user token signature
                // algo
                *(DWORD*)ptr = 0xffffffff ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;
                // signature name : opcua null string                
                *(DWORD*)ptr = 0xffffffff ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;




    _internal_buf_size = ptr - debut;
    mylog(2,"OPCUACommand::ActivateSessionRequest _internal_buf_size=%d...",_internal_buf_size);
    //*(DWORD*)ptr_len1 = (ptr-debut);
        *ptr_len1 = (int)_internal_buf_size%256; ptr_len1++ ;
        *ptr_len1 = (int)_internal_buf_size/256; ptr_len1++ ;
        *ptr_len1 = (int)_internal_buf_size/(256*256); ptr_len1++ ;
        *ptr_len1 = (int)_internal_buf_size/(256*256*256); ptr_len1++ ;    
    return _internal_buf_size ;
}