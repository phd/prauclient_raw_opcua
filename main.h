#include <stdio.h>      /* for printf() and fprintf() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h> /* for socket(), connect(), sendto(), and recvfrom() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <openssl/md5.h>
#include <limits.h>
#include <netdb.h>
#include <netinet/in.h>
#include <ctype.h>
#include <syslog.h>
#include <math.h>
#include <arpa/inet.h> //inet_addr
#include <sys/socket.h>    //socket
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <arpa/inet.h> // for inet_ntoa()
#include <pcap.h>
#include <net/ethernet.h>
#include <netinet/ip_icmp.h>   //Provides declarations for icmp header
#include <netinet/udp.h> //Provides declarations for udp header
#include <netinet/tcp.h> //Provides declarations for tcp header
#include <netinet/ip.h>  //Provides declarations for ip header
#include <arpa/inet.h> //inet_addr
#include <sys/socket.h>    //socket
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <arpa/inet.h> // for inet_ntoa()
#include <pcap.h>
#include <net/ethernet.h>
#include <netinet/ip_icmp.h>   //Provides declarations for icmp header
#include <netinet/udp.h> //Provides declarations for udp header
#include <netinet/tcp.h> //Provides declarations for tcp header
#include <netinet/ip.h>  //Provides declarations for ip header

#include <string>
#include <map>
#include <iostream>
#include <vector>
#include <sstream>
#include <QtCore>
#include <QString>
#include <QThread>
#include <QQueue>
#include <QMutex>
#include <QMap>
#include <string>
#include <map>
#include <iostream>
#include <vector>
#include <sstream>
#include <QCoreApplication>
#include <QTimer>
#include <QThread>
#include <QTextStream>
#include <QDebug>
#include <QString>
#include <QStringList>
#include <QLinkedList>
#include <iostream>
#include <QEvent>
#define BYTE unsigned char
#define WORD unsigned short
#define DWORD unsigned long
#define SOCKET int
#define SOCKET_ERROR -1
#define MIN(a,b) (a<b?a:b)
#define Sleep(a) usleep(a*1000*1000)
#define APP_NAME        "opcua client"
#define ONE_MILLISEC_USLEEP (1000)
#define ONE_SEC_USLEEP (1000*1000)
#define opcua_BUF_MAX_SIZE 1024

#include "message_identifiers.h"
#include "status_codes.h"

using namespace std; // std:string


class myCoreApp : public QCoreApplication
{
    Q_OBJECT
public:
    myCoreApp(int & argc, char ** argv):QCoreApplication(argc,argv){}
    //explicit myCoreApp(int & argc, char ** argv);
    bool event(QEvent *event);
    void beforeExiting();
public slots:
};

class myMainClass : public QObject
{
    Q_OBJECT
private:
    QCoreApplication *app;
    bool _stopRunning;
public:
    explicit myMainClass(QObject *parent = 0);
    void quit();/// Call this to quit application
signals:
    void finished();/// Signal to finish, this is connected to Application Quit
public slots:
    void run();/// This is the slot that gets called from main to start everything but, everthing is set up in the Constructor
    void aboutToQuitApp();/// slot that get signal when that application is about to quit
};

#define OPCUA_BUF_MAX_SIZE 1500
#define SEQUENCE_TAG 16
#define INTEGER_TAG 2
#define ENUMERATED_TAG 10

//#define GET_ENDPOINT_REQUEST 428
//  GET_ENDPOINTS_REQUEST  = 0x1ac, // 428

class OPCUACommand{
public:
    static DWORD invokeid;
    static DWORD security_token_id;
    static DWORD security_seq_number;
    static DWORD security_req_id;


    static QMap<BYTE,QByteArray>        _extensionByInvokeid;
    static QMap<QByteArray,QByteArray>  _crossrefByextension;

    OPCUACommand(){memset(_internal_buf,0,sizeof(_internal_buf));_internal_buf_size=0;}
    OPCUACommand(BYTE* data,unsigned int len){
        _internal_buf_size = len;
        //if ( len > sizeof(_internal_buf) ) _internal_buf_size = sizeof(_internal_buf)-1;
        if ( data!=NULL && _internal_buf_size>0 )
            memcpy(_internal_buf,data,_internal_buf_size);
    }
    ~OPCUACommand(){}
    BYTE            _internal_buf[OPCUA_BUF_MAX_SIZE];
    unsigned short  _internal_buf_size;
    QByteArray      _originalData;
    QByteArray      _data;

    QString         _answeringDevice;
    QString         _callingDevice;

    unsigned short  _lastReportEventInvokeID;
    int             _lastReportEventcrossRefIdLen;
    QByteArray      _lastReportEventcrossRefId;

    // TAG Type/lenght/Vlaue
    QByteArray      _TLVValue;
    int             _TLVLength;
    int             _TLVTag;
    //void hexdump(BYTE* ptr, unisgned short len);    
    void hexDump();   
     
    int Hello(QString endpoint_url);
    int OpenSecureChannel(QString policy);
    int SecureMsg(QString);
    int GetEndPointRequest(DWORD channel_id, DWORD security_id, DWORD security_seq_number, DWORD security_req_id, QString endpoint_url);
    
    int CreateSessionRequest();
    int ActivateSessionRequest();

    int ReadRequest(WORD node_id_to_read_identifier_numeric, BYTE node_id_to_read_namespace_index);
    int BrowseRequest(WORD node_id_to_read_identifier_numeric, BYTE node_id_to_read_namespace_index);
    int CreateSubscriptionRequest(QString str, DWORD channel_id);    
};

class OPCUALink : public QThread
{
public:
    bool init();
    bool startNetwork();

    OPCUALink(){
        _isConnected=false;
        _is_stopped=false;
        opcua_RTP_PORT_START = 40000;
        opcua_RTP_PORT_END = 60000;
        _next_opcua_rtp_port_pair=opcua_RTP_PORT_START;
    }

    // params.ini
    quint16                     opcua_RTP_PORT_START;
    quint16                     opcua_RTP_PORT_END;
    quint16                     _OPCUASERVER_opcua_PORT;
    QString                     _OPCUASERVER_IP_ADDRESS;
    QString                     _opcua_local_ip_addr; /* OXE server address */

    bool                        _is_stopped;
    pthread_t                   _thread_id;
    bool                        _isConnected;
    int                         _sock;
    struct sockaddr_in          _oxe_server;

    quint16                     _next_opcua_rtp_port_pair;
    // link keep alive with status report inquiry from oxe every 30 seconds
    int                         _lastReportStatusCrossRefIdLen;
    QByteArray                  _lastReportStatusCrossRefId;

    bool sendHello()    ;
    bool sendFrame(OPCUACommand* pCmd);
    bool processReceivedCommand(OPCUACommand* pCmd);

    DWORD                       _SecurityRequestId;
    DWORD                       _SecuritySequenceNumber;
    DWORD                       _SecurityTokenId;
    DWORD                       _SecureChannelId;
    BYTE                        _timestamp[8];

    BYTE                        _authTokenGUID[16];
    WORD                        _authTokenNS;

    DWORD                       _requestHandle;
    DWORD                       _session_node_id;
    DWORD                       _NS_index_session_node_id;

    void setTimestamp(BYTE* ts);
};

void mylog(unsigned char level,const char* fmt, ...);
void* opcuaDataListeningThread(void* ctx);
void hexdump(unsigned char* buf, int len);
unsigned char hex2bin(char c);

const size_t MESSAGE_TYPE_SIZE = 3;
const char MESSAGE_TYPE_HELLO[MESSAGE_TYPE_SIZE]       = {'H', 'E', 'L'};
const char MESSAGE_TYPE_ACKNOWLEDGE[MESSAGE_TYPE_SIZE] = {'A', 'C', 'K'};
const char MESSAGE_TYPE_ERROR[MESSAGE_TYPE_SIZE]       = {'E', 'R', 'R'};
const char MESSAGE_TYPE_MESSAGE[MESSAGE_TYPE_SIZE]     = {'M', 'S', 'G'};
const char MESSAGE_TYPE_OPEN[MESSAGE_TYPE_SIZE]        = {'O', 'P', 'N'};
const char MESSAGE_TYPE_CLOSE[MESSAGE_TYPE_SIZE]       = {'C', 'L', 'O'};

extern OPCUALink myOPCUAlink;
