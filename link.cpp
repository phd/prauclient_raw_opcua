#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <syslog.h>
#include <QDebug>
#include <QDir>
#include <sys/ioctl.h>
#include <net/if.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "main.h"
//#include "guid.h"
#include "status_codes.h"

bool OPCUALink::sendHello()
{
    if (0)
    {
        char csta_tsapi_init[] = "0053605180020780a10706052b0c008134a70f040d5453455256455245434d415632be31282f06072b0c00815a8148a0243022030206c0301a800403ffffff810602ffffffffff82020300830206c0840203f83000";
        unsigned char csta_array[1024];
        int csta_array_len = 0;
        for (unsigned int j = 0; j < strlen(csta_tsapi_init); j += 2)
        {
            csta_array[csta_array_len] = (hex2bin(csta_tsapi_init[j]) << 4) | hex2bin(csta_tsapi_init[j + 1]);
            //mylog(2,"try sending 0x%c%c = %02x (%d)\n",csta_init[j] ,csta_init[j+1] ,csta_array[csta_array_len], csta_array_len);
            //int ret_send = send(sock, &cur_byte, 1, 0);
            csta_array_len++;
        }
        OPCUACommand *pCmd = new OPCUACommand(csta_array, csta_array_len);
    }
    else
    {

        OPCUACommand *pCmd = new OPCUACommand();

        if (pCmd)
        {
            pCmd->Hello("opc.tcp://192.168.8.183:8383/MTRAUServer");
            pCmd->hexDump();
            //cstaLink->_commandsToSend.push(pCmd);
            int ret_send = send(_sock, pCmd->_internal_buf, pCmd->_internal_buf_size, 0);
            mylog(4, "OPCUA send %d/%d bytes\n", ret_send, pCmd->_internal_buf_size);
        }
    }
    return true;
}

bool OPCUALink::processReceivedCommand(OPCUACommand* pCmd)
{
    return true;
}

bool OPCUALink::sendFrame(OPCUACommand* pCmd)
{
    if  (pCmd ){
        //pCmd->Hello("opc.tcp://192.168.8.183:8383/MTRAUServer");                
        //pCmd->hexDump();
        //cstaLink->_commandsToSend.push(pCmd);
        int ret_send = send(_sock, pCmd->_internal_buf, pCmd->_internal_buf_size, 0);
        mylog(4,"socketSend %d/%d bytes\n",ret_send,pCmd->_internal_buf_size);
    }    
}
bool OPCUALink::init()
{
    _SecuritySequenceNumber = 1;
    _SecurityRequestId = 1;
    _SecurityTokenId = 1;
    _SecureChannelId = 1;
    
    _opcua_local_ip_addr = "127.0.0.1";
    _OPCUASERVER_IP_ADDRESS = "127.0.0.1";
    _OPCUASERVER_opcua_PORT = 8383;

    QSettings settings("params.ini", QSettings::IniFormat);
    qDebug() << settings.fileName();
    _opcua_local_ip_addr = settings.value("local_address").toString();
    qDebug() << _opcua_local_ip_addr;
    _OPCUASERVER_IP_ADDRESS = settings.value("opcua_server_address").toString();
    qDebug() << _OPCUASERVER_IP_ADDRESS;
    _OPCUASERVER_opcua_PORT = settings.value("opcua_server_port").toUInt();
    qDebug() << _OPCUASERVER_opcua_PORT;

    //opcua_RTP_PORT_START = settings.value("opcua_rtp_port_start").toUInt();
    //opcua_RTP_PORT_END = settings.value("opcua_rtp_port_end").toUInt();
    return true;
}
void OPCUACommand::hexDump()
{
    fprintf(stderr,"--> OPCUACommand _internal_buf_size[%d]\n",_internal_buf_size);
    char* str = (char*)malloc(3*_internal_buf_size+1);
    if ( str ){
        int str_len=0;
        for( int k=0,m=0; k< _internal_buf_size; k++,m++){
            str_len += sprintf(str+str_len, "%02X-", _internal_buf[k]);
        }
        fprintf(stderr,"OPCUACommand [%s]\n",str);
    }
}

bool OPCUALink::startNetwork()
{
    mylog(2,"OPCUALink::startNetwork");

        _sock = socket(AF_INET , SOCK_STREAM , 0);
        if (_sock == -1){
            mylog(3,"Could not create socket");
            return false;
        }
        mylog(3,"Socket created");
        
        /*if(fcntl(_sock, F_GETFL) & O_NONBLOCK) {
            mylog(3,"socket is non-blocking");
        }
        
        mylog(3,"Put the socket in non-blocking mode:");
        if( fcntl(_sock, F_SETFL, fcntl(_sock, F_GETFL) | O_NONBLOCK) < 0) {
            mylog(3,"ERROR Put the socket in non-blocking mode:");
        }*/        

        memset(&_oxe_server,0,sizeof(_oxe_server)); //struct sockaddr_in
        _oxe_server.sin_addr.s_addr = inet_addr(_OPCUASERVER_IP_ADDRESS.toLatin1().constData());
        _oxe_server.sin_family = AF_INET;
        _oxe_server.sin_port = htons( _OPCUASERVER_opcua_PORT );

        if ( ::connect(_sock , (struct sockaddr *)&_oxe_server , sizeof(_oxe_server)) < 0)
        {   //Connect to remote server
            mylog(3,"connect failed. Error");
            
        }else{
            mylog(3,"OK socket connected %d",_sock);
            _is_stopped = false;

            pthread_create(&_thread_id, NULL, opcuaDataListeningThread, (void*)this);

            mylog(2,"envoi trame HELLO OPCUA....");

            sendHello();

            /*while(1){
                char carcou;
                //mylog(2,"-> recv on socket %d....", _sock);
                int ret = recv( _sock, &carcou, 1, 0  );
                mylog(2,"rx %d: [%02X]\n",ret, (unsigned char)carcou);            
                if (ret>0){
                    //mylog(2,"rx %d:%02X",ret, (unsigned char)carcou);            
                    if ( isprint(carcou) ) printf("(%c)",carcou);
                    else printf("(%02X)",(unsigned char)carcou);
                }

            }*/

            sleep(4);

            if(1){
                OPCUACommand* pCmd = new OPCUACommand();
                if ( pCmd ) {
                    pCmd->OpenSecureChannel("http://opcfoundation.org/UA/SecurityPolicy#None");
                    this->sendFrame(pCmd);
                }
            }

            sleep(4);

            /*if(0){
                OPCUACommand* pCmd = new OPCUACommand();
                if ( pCmd ) {
    //int SecureMsgGetEndPointRequest(DWORD channel_id, DWORD security_id, DWORD security_seq_number, DWORD security_req_id, QString endpoint_url);

                    DWORD channel_id=7970;
                    DWORD security_id = 1;
                    DWORD security_seq_number=2;
                    DWORD security_req_id=2;
                    QString endpoint_url = "opc.tcp://192.168.8.183:8383/MTRAUServer";
                    pCmd->SecureMsgGetEndPointRequest(channel_id,security_id,
                        security_seq_number,security_req_id,endpoint_url);
                    this->sendFrame(pCmd);
                }
            }*/

        }
    //start();
    return true;
}

void OPCUALink::setTimestamp(BYTE* ts){memcpy(&_timestamp[0], ts, 8 ) ;}

void* opcuaDataListeningThread(void* ctx)
{
    if ( ctx==NULL) return NULL;
    OPCUALink* opcuaLink = (OPCUALink*)ctx;
    mylog(5,"--> opcuaDataListeningThread on socket %d\n",opcuaLink->_sock);

    while (!opcuaLink->_is_stopped )
    {
        /*
        char msg='B';
        int ret_send = send(opcuaLink->_sock, &msg, sizeof(msg), 0);
        mylog(2,"ret_send [%d]\n",ret_send);
        if (ret_send == -1) {
            mylog(2,"err send()\n");
            continue;
        }
        char carcou;        */
        /*int ret = recv( opcuaLink->_sock, &carcou, 1, 0  );
        mylog(2,"rx %d:%02X\n",ret, (unsigned char)carcou);

        if ( 'P' != carcou ){
            mylog(2,"bad answer from OXE");
            continue;
        }else{
            mylog(2,"received [P] from OXE\n");
            opcuaLink->_isConnected = true;
            //opcuaLink->startAuth();
        }*/       
        mylog(2,"OPCUALink recv on socket %d", opcuaLink->_sock);
        while(opcuaLink->_sock > 0 )//        while( 1 )
        {
            #define MAX_REPLY_HEADER_SIZE 3 // OPN MSG
            int idx=0;
            int ret_recv=0;
            unsigned char server_reply[MAX_REPLY_HEADER_SIZE+1]={0};
            unsigned char carcou1='.';
            unsigned char carcou2='.';
            unsigned char carcou3='.';
            unsigned char chunkType='.';
            unsigned int frameSize=0;
            unsigned char buf[10*1024]={0};

            ret_recv = recv(opcuaLink->_sock , &carcou1 , 1 , 0);
            if ( ret_recv>0 ){
                //mylog(2,"A)rx %d: [%02X]\n",ret_recv, (unsigned char)carcou1);            
                ret_recv = recv(opcuaLink->_sock , &carcou2 , 1 , 0);
                if ( ret_recv>0 ){
                    //mylog(2,"B)rx %d: [%02X]\n",ret_recv, (unsigned char)carcou2);            
                    ret_recv = recv(opcuaLink->_sock , &carcou3 , 1 , 0);
                    if ( ret_recv>0 ){
                        //mylog(2,"C)rx %d: [%02X]\n",ret_recv, (unsigned char)carcou3);            
                        QString reply; 
                        reply+=carcou1;
                        reply+=carcou2;
                        reply+=carcou3;
                        //qDebug() << reply;
                        if ( (reply == QString("ACK")) 
                            || (reply == QString("OPN")) 
                            || (reply == QString("ERR"))
                            || (reply == QString("MSG"))
                        ) 
                        {
                            mylog(2,"--------------opcua recv [%c%c%c] string -------------------",carcou1,carcou2,carcou3);

                            ret_recv = recv(opcuaLink->_sock , &chunkType , 1 , 0);
                            mylog(2,"D)rx chunkType %d BYTE(s): [%c]\n",ret_recv, (unsigned char)chunkType);            

                            ret_recv = recv(opcuaLink->_sock , &frameSize , sizeof(frameSize) , 0);
                            mylog(2,"E)rx frameSize %d: [%d]\n",ret_recv, (unsigned char)frameSize);            
                            frameSize -= sizeof(carcou1);
                            frameSize -= sizeof(carcou2);
                            frameSize -= sizeof(carcou3);
                            frameSize -= sizeof(chunkType);
                            frameSize -= sizeof(frameSize);

                            int idx=0;
                            while ( frameSize>0  && idx<frameSize && idx< (sizeof(buf)-1) )  {
                                ret_recv = recv(opcuaLink->_sock , &buf[idx] , 1 , 0);
                                if ( ret_recv>0 ){
                                    //mylog(2,"F)buf[%d]=[%02X]\n",idx, (unsigned char)buf[idx]);            
                                    idx++;
                                }      
                            }
                            //mylog(2,"F)buf[%d]=[%02X]\n",idx-1, (unsigned char)buf[idx-1]);            
                            mylog(2,"------------------ RX OPCUA FRAME (%d bytes) -----------------------",idx);    
                            if ( reply == QString("ACK") ){ // acknowledge message

                                BYTE* ptr = &buf[0];// + 2;
                                DWORD version = *(DWORD*)(ptr);ptr+=4;
                                DWORD ReceiveBufferSize = *(DWORD*)(ptr);ptr+=4;
                                DWORD SendBufferSize = *(DWORD*)(ptr);ptr+=4;
                                DWORD MaxMessageSize = *(DWORD*)(ptr);ptr+=4;
                                DWORD MaxChunkCount = *(DWORD*)(ptr);ptr+=4;
                                
                                mylog(2,"FRAME ACK version:%d",version);    
                                mylog(2,"FRAME ACK ReceiveBufferSize:%d",ReceiveBufferSize);    
                                mylog(2,"FRAME ACK SendBufferSize:%d",SendBufferSize);    
                                mylog(2,"FRAME ACK MaxMessageSize:%d",MaxMessageSize);    
                                mylog(2,"FRAME ACK MaxChunkCount:%d",MaxChunkCount);    
                            }
                            if ( reply == QString("MSG") ){ 
                                BYTE* ptr = &buf[0];
                                DWORD SecureChannelId = *(DWORD*)(ptr);ptr+=4;
                                mylog(2,"FRAME MSG SecureChannelId:%d",SecureChannelId);   
                                DWORD securityTokenId = *(DWORD*)(ptr);ptr+=4;
                                mylog(2,"FRAME MSG securityTokenId:%X",securityTokenId);    
                                DWORD securitySequenceNumber = *(DWORD*)(ptr);ptr+=4;
                                mylog(2,"FRAME MSG securitySequenceNumber:%X",securitySequenceNumber);    
                                DWORD securityRequestId = *(DWORD*)(ptr);ptr+=4;
                                mylog(2,"FRAME MSG securityRequestId:%X",securityRequestId);    

                                // opcua service : encodable object
                                    // typeid
                                        //nodeId EncodingMask : 0x01 = 4 bytes encoded numeric
                                        BYTE EncodingMask = *(BYTE*)(ptr);ptr+=1;
                                        mylog(2,"nodeId EncodingMask:%X",EncodingMask);    
                                        // nodespace name index : 0
                                        BYTE nodespaceIndex = *(BYTE*)(ptr);ptr+=1;
                                        mylog(2,"nodeId nodespaceIndex:%X",nodespaceIndex);    
                                        // node identifier numeric 431 GetEndponitsresponse
                                        WORD nodeId_Identifier = *(WORD*)(ptr);ptr+=2;
                                        mylog(2,"nodeId nodeId_Identifier:%X",nodeId_Identifier);    

                                        if (nodeId_Identifier == OpcUaMessageId::READ_RESPONSE /*431*/){
                                            mylog(2,">>>>>>>>---------- OpcUaMessageId::READ_RESPONSE ----------");
                                        }
                                        if (nodeId_Identifier == OpcUaMessageId::BROWSE_RESPONSE /*431*/){
                                            mylog(2,">>>>>>>>---------- OpcUaMessageId::BROWSE_RESPONSE ----------");
                                        }
                                        if (nodeId_Identifier == OpcUaMessageId::ACTIVATE_SESSION_RESPONSE /*431*/){
                                            mylog(2,">>>>>>>>---------- OpcUaMessageId::ACTIVATE_SESSION_RESPONSE ----------");

                                            /*******************************************************************************************************
                                             * 
                                             * ICI SESSION ACTIVE !!!!! TODO : READ REQUEST avec array node to read 
                                             * 
                                             * *******************************************************************************************************/

                                            usleep(ONE_SEC_USLEEP);
                                            OPCUACommand* pCmd2 = new OPCUACommand();
                                            if ( pCmd2 ) {
                                                pCmd2->ReadRequest(2255,0);
                                                pCmd2->hexDump();
                                                opcuaLink->sendFrame(pCmd2);                                                    
                                            }    

                                            OPCUACommand* pCmd3 = new OPCUACommand();
                                            if ( pCmd3 ) {
                                                pCmd3->BrowseRequest(29,55);
                                                pCmd3->hexDump();
                                                opcuaLink->sendFrame(pCmd3);                                                    
                                            }    

                                            mylog(2,"<<<<<<<----------- OpcUaMessageId::ACTIVATE_SESSION_RESPONSE ----------");
                                        }
                                        else if (nodeId_Identifier == OpcUaMessageId::CREATE_SESSION_RESPONSE /*431*/){
                                            mylog(2,">>>>>>>>----------- OpcUaMessageId::CREATE_SESSION_RESPONSE ----------");

                                            ptr+=8; // timestamp
                                            // request_hanlde
                                            DWORD rx_request_hanlde = *(DWORD*)(ptr);ptr+=4;
                                            mylog(2,"nodeId rx_request_hanlde:%d",rx_request_hanlde);    
                                            opcuaLink->_requestHandle = rx_request_hanlde;

                                            DWORD service_result = *(DWORD*)(ptr);ptr+=4;
                                            mylog(2,"nodeId service_result:%d",service_result);    
                                            if ( service_result == 0 ) mylog(2,"GOOD");

                                            BYTE service_diagnostic_info = *(BYTE*)(ptr);ptr+=1;
                                            mylog(2,"service_diagnostic_info:%X",service_diagnostic_info);                                               

                                            DWORD string_table_size = *(DWORD*)(ptr);ptr+=4;
                                            mylog(2,"string_table_size:%d",string_table_size);    
                                            //ptr += string_table_size;

                                            // additional header expand node id
                                            WORD expanded_node_id = *(WORD*)(ptr);ptr+=2;
                                            mylog(2,"expanded_node_id:%d",expanded_node_id);   
                                            BYTE encoding_mask = *(BYTE*)(ptr);ptr+=1;
                                            mylog(2,"expanded_node_id_encoding_mask:%d",encoding_mask);   

                                            // session nodeid
                                            BYTE encoding_mask3 = *(BYTE*)(ptr);ptr+=1;
                                            BYTE NS_index_session_node_id = *(BYTE*)(ptr);ptr+=1;
                                            mylog(2,"NS_index_session_node_id:%d",NS_index_session_node_id);   
                                            WORD session_node_id = *(WORD*)(ptr);ptr+=2;
                                            mylog(2,"session_node_id:%d NS_index:%d",session_node_id,NS_index_session_node_id);    

                                            // auth token
                                            BYTE encoding_mask2 = *(BYTE*)(ptr);ptr+=1;
                                            mylog(2,"auth token encoding_mask:%d",encoding_mask2);   
                                            WORD authTokenNS = *(WORD*)(ptr);ptr+=2;
                                            mylog(2,"authTokenNS:%d",authTokenNS);   
                                            opcuaLink->_authTokenNS = authTokenNS;
                                            if ( encoding_mask2 == 4 ){// GUID                                                
                                                char str[1024];unsigned int str_len=0;
                                                //memcpy(&opcuaLink->_authTokenGUID[0], ptr, 16);
                                                for (int k=0;k<16;k++) {
                                                    opcuaLink->_authTokenGUID[k] = *(BYTE*)ptr;
                                                    str_len += sprintf(str+str_len, "%02X-", *(BYTE*)ptr);                                                                                                
                                                    //mylog(2, "--> CreateSessionResponse _authTokenGUID= %02X %02X ",*(BYTE*)ptr, myOPCUAlink._authTokenGUID[k] );
                                                    ptr++;
                                                }                                                
                                                mylog(2,"_authTokenGUID:%s",str);    
                                                mylog(2,"_authTokenNS:%d",opcuaLink->_authTokenNS);    
                                                ptr+=16;

                   
                                            }//else exit(-1);
                                            mylog(2,"<<<<<<<<---------- OpcUaMessageId::CREATE_SESSION_RESPONSE ----------");

                                            if(1){
                                                usleep(ONE_SEC_USLEEP);
                                                OPCUACommand* pCmd3 = new OPCUACommand();
                                                if ( pCmd3 ) {

                                                    pCmd3->ActivateSessionRequest();
                                                    //pCmd2->hexDump();
                                                    opcuaLink->sendFrame(pCmd3);                                                    
                                                } 
                                            }                                             

                                        }
                                        else if (nodeId_Identifier == OpcUaMessageId::GET_ENDPOINTS_RESPONSE /*431*/){
                                            mylog(2,"----------- OpcUaMessageId::GET_ENDPOINTS_RESPONSE ----------");                                            
                                        }else {
                                            mylog(2,"----------- OpcUaMessageId::UNHANDLED %X ----------",nodeId_Identifier);                                            
                                        }                                        
                            }

                            if ( reply == QString("ERR") ){ // openSecureChannelResponse

                                BYTE* ptr = &buf[0];// + 2;
                                DWORD Error = *(DWORD*)(ptr);ptr+=4;
                                mylog(2,"FRAME ERR Error:%08X",Error);    
                                
                                if ( Error == OpcUaStatusCode::BadSessionIdInvalid /*0x80250000*/ ){
                                    mylog(2,"FRAME ERR OpcUa::BadSessionIdInvalid :%08X",Error); 
                                }
                                
                                unsigned int SecurityPolicyLen = *(DWORD*)(ptr);ptr+=4;
                                mylog(2,"FRAME ACK SecurityPolicyLen:%d",SecurityPolicyLen);    

                                DWORD Reason = *(DWORD*)(ptr);ptr+=4;
                                mylog(2,"FRAME ACK Reason:%X",Reason);    

                            }

                            if ( reply == QString("OPN") ){ // openSecureChannelResponse

                                BYTE* ptr = &buf[0];// + 2;
                                DWORD rxSecureChannelId = *(DWORD*)(ptr);ptr+=4;
                                mylog(2,"RX FRAME openSecureChannelResponse --> SecureChannelId:%d",rxSecureChannelId);    
                                opcuaLink->_SecureChannelId = rxSecureChannelId;
                                
                                unsigned int SecurityPolicyLen = *(DWORD*)(ptr);ptr+=4;
                                mylog(2,"FRAME ACK SecurityPolicyLen:%d",SecurityPolicyLen);    
                                for (DWORD j=0;j<SecurityPolicyLen;j++){
                                    //mylog(2,"j=%d [%c]",j,*ptr++);//mylog(2,"SecurityPolicyLen:%d",SecurityPolicyLen);    
                                    if ( j >= SecurityPolicyLen) {
                                        mylog(2,"SecurityPolicyLen:%d",SecurityPolicyLen);    
                                        mylog(2,"break j=%d",j);
                                        break;
                                    }                                    
                                }
                                ptr+=SecurityPolicyLen;

                                DWORD SenderCertificate = *(DWORD*)(ptr);ptr+=4;
                                mylog(2,"SenderCertificate:%X",SenderCertificate);    
                                DWORD ReceiverCertificate = *(DWORD*)(ptr);ptr+=4;
                                mylog(2,"ReceiverCertificate:%X",ReceiverCertificate);    

                                DWORD rxSequenceNumber = *(DWORD*)(ptr);ptr+=4;
                                mylog(2,"FRAME ACK SequenceNumber:%d",rxSequenceNumber);    
                                opcuaLink->_SecuritySequenceNumber = rxSequenceNumber;

                                DWORD rxRequestId = *(DWORD*)(ptr);ptr+=4;
                                mylog(2,"FRAME ACK RequestId:%d",rxRequestId);    
                                opcuaLink->_SecurityRequestId = rxRequestId;

                                if(1){
                                    usleep(ONE_SEC_USLEEP);
                                    OPCUACommand* pCmd2 = new OPCUACommand();
                                    if ( pCmd2 ) {
                                        opcuaLink->_SecurityTokenId = 1;
                                        opcuaLink->_SecuritySequenceNumber++;
                                        opcuaLink->_SecurityRequestId++;
                                        
                                        pCmd2->CreateSessionRequest( );                             
                                        pCmd2->hexDump();
                                        opcuaLink->sendFrame(pCmd2);
                                    }
                                }
                                if(0){
                                    usleep(ONE_SEC_USLEEP);
                                    OPCUACommand* pCmd2 = new OPCUACommand();
                                    if ( pCmd2 ) {
                                        DWORD security_id = 1;
                                        DWORD security_seq_number=2;
                                        DWORD security_req_id=2;
                                        QString endpoint_url = "opc.tcp://192.168.8.183:8383/MTRAUServer";
                                        pCmd2->GetEndPointRequest(
                                            opcuaLink->_SecureChannelId,
                                            security_id,
                                            security_seq_number,
                                            security_req_id,
                                            endpoint_url);                                        
                                        pCmd2->hexDump();
                                        opcuaLink->sendFrame(pCmd2);
                                    }
                                }

                            }                            
                            //CopcuaCommand* pCmd = new CopcuaCommand(server_reply,ret_recv);
                            //pCmd->hexdump();
                            //opcuaLink->_commandsReceived.push(pCmd);
                        }
                    }
                }
            }else if ( ret_recv==0 ){
                usleep(ONE_SEC_USLEEP);
            }
        }
    }
    mylog(5,"<-- opcuaDataListeningThread ending normally\n");
    return NULL;
}

