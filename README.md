* prauclient
---
PAU OPCUA client

Connexion avec SAPEX en TCP

Connexion au serveur OPCUA de MT pour lire et s'abonner aux TAG de PAU


Le protocole utilisé est un protocole standard OPC UA en binaire TCP. Le serveur OPC, propriété de MT est
en écoute sur le port 8383, et porte le nom de « MTRAUServer ». Il est accessible via l’URL :

**opc.tcp://[ip_du_serveur_MTRAUServer]:8383/MTRAUServer**

ip_du_serveur_MTRAUServer: désigne l’adresse IP de MTRAUServer, configurable sur l’outil de configuration

Les échanges entre client et serveur OPC seront sécurisés par l’utilisation de login/password. Ces identifiants
de connexion doivent être connus des deux parties de l’interface. La méthode de configuration de ces
paramètres n’est pas, à ce stade, clairement définie. Pour dialoguer avec MTRAUServer, le client doit
utiliser les paramètres de chiffrements ci-après : Security policy : Basic256sha256 Message security mode : Sign & encrypt