#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <syslog.h>
#include <QDebug>
#include <QDir>
#include <sys/ioctl.h>
#include <net/if.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "main.h"

int OPCUACommand::CreateSubscriptionRequest(QString str, DWORD channel_id)
{
    mylog(5,"OPCUACommand::CreateSubscriptionRequest...");
    //mylog(5,"ip1=%02X",ip1);    mylog(5,"ip2=%02X",ip2);    mylog(5,"ip3=%02X",ip3);    mylog(5,"ip4=%02X",ip4);

    BYTE* debut = _internal_buf;// + 2;
    BYTE* ptr = debut;
    
    *ptr++ = 'M'; // H
    *ptr++ = 'S'; // E
    *ptr++ = 'G'; // L
    *ptr++ = 'F'; // F

    // message size 4 BYTES    
    BYTE* ptr_len1 = ptr; 
    *ptr = 0x11; ptr++ ;
    *ptr = 0x22; ptr++ ;
    *ptr = 0x33; ptr++ ;
    *ptr = 0x44; ptr++ ;

   mylog(2,"--> CreateSubscriptionRequest %d",channel_id);
        BYTE* ptr_tmp = ptr;
                *ptr = channel_id % 256; ptr++ ;
                *ptr = channel_id / 256; ptr++ ;
                *ptr = channel_id / 256*256; ptr++ ;
                *ptr = channel_id / 256*256*256; ptr++ ;
    mylog(2,"--> CreateSubscriptionRequest %02X %02X %02X %02X",ptr_tmp[0],ptr_tmp[1],ptr_tmp[2],ptr_tmp[3]);


    // security_token_id 4 BYTES    0x22 0x1f 0x00 0x00
    *(DWORD*)ptr = security_token_id; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

    // security_seq_number 4 BYTES    0x22 0x1f 0x00 0x00
    *(DWORD*)ptr = security_seq_number; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

    // security_req_id  4 BYTES    0x22 0x1f 0x00 0x00
    *(DWORD*)ptr = security_req_id ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

    security_seq_number++;
    security_req_id++;

    /// ---> OPCUA SERVICE
        // type_id: expandedNodeId
            // nodeid encoding mask
            *ptr = 1; ptr++ ; // 4 bytes encoded numeric
            // nodeid namespace index
            *ptr = 0; ptr++ ;
            // getendpointrequest: 428
            *(WORD*)ptr = OpcUa::CREATE_SUBSCRIPTION_REQUEST ;ptr++ ;ptr++ ; // 0xAC 0x01    
        // read request
            // read request header
                // authenticationToken: nodeid
                *ptr = 0x04; ptr++ ; // encoding mask GUID 0x04
                *ptr = 0x01; ptr++ ; // namespace index
                *ptr = 0x00; ptr++ ; // namespace index
                
                *ptr = 0xd6; ptr++ ; // GUID 16 bytes
                *ptr = 0xc0; ptr++ ; // GUID
                *ptr = 0x77; ptr++ ; // GUID
                *ptr = 0xfe; ptr++ ; // GUID
                *ptr = 0x13; ptr++ ; // GUID
                *ptr = 0xf9; ptr++ ; // GUID
                *ptr = 0x26; ptr++ ; // GUID
                *ptr = 0x42; ptr++ ; // GUID
                *ptr = 0x81; ptr++ ; // GUID
                *ptr = 0x20; ptr++ ; // GUID
                *ptr = 0x04; ptr++ ; // GUID
                *ptr = 0x8b; ptr++ ; // GUID
                *ptr = 0x65; ptr++ ; // GUID
                *ptr = 0x8a; ptr++ ; // GUID
                *ptr = 0x6b; ptr++ ; // GUID
                *ptr = 0xee; ptr++ ; // GUID

            // timestamp 8 bytes                
            *ptr = 0x80; ptr++ ;
            *ptr = 0xeb; ptr++ ;
            *ptr = 0xad; ptr++ ;
            *ptr = 0xe6; ptr++ ;                
            *ptr = 0x15; ptr++ ;
            *ptr = 0xc1; ptr++ ;
            *ptr = 0xd6; ptr++ ;
            *ptr = 0x01; ptr++ ;                

                // request handle 4 BYTES    0x22 0x1f 0x00 0x00
                *(DWORD*)ptr = 0x000f490b ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

                // resturn diagnostic 4 BYTES
                *(DWORD*)ptr = 0x00 ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

                // audit entry id 4 BYTES
                *(DWORD*)ptr = 0xffffffff ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

                // timeoutHint 4 BYTES
                *(DWORD*)ptr = 0x00001388 ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

            // additional header: extension object
            *ptr = 0x00; ptr++ ;  // typeid expanded node id
            *ptr = 0x00; ptr++ ;
            *ptr = 0x00; ptr++ ; // encoding mask               

            *ptr = 0x00; ptr++ ; // max age               
            *ptr = 0x00; ptr++ ; // max age               
            *ptr = 0x00; ptr++ ; // max age               
            *ptr = 0x00; ptr++ ; // max age                           
            *ptr = 0x00; ptr++ ; // max age               
            *ptr = 0x00; ptr++ ; // max age               
            *ptr = 0x00; ptr++ ; // max age               
            *ptr = 0x00; ptr++ ; // max age               

                // timestampTo return 4 BYTES
                *(DWORD*)ptr = 0x01 ; ptr++ ;ptr++ ;ptr++ ;ptr++ ; // server (0x0000001)

                // nodes to read
                    // array size 4 BYTES
                        *(DWORD*)ptr = 0x01 ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

                // read value id [0]
                    // node id
                    *ptr = 0x01; ptr++ ; // 4 bytes encoded numeric
                    *ptr = 0x00; ptr++ ; // namespace index

                    *(WORD*)ptr = 2259 ; ptr++ ;ptr++ ; // identifier numeric

                    // attribute value id
                    *(DWORD*)ptr = 0x0000000d ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

                    // index range
                    *(DWORD*)ptr = 0xffffffff ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;

                    // dataencoding : qualified name
                    *(WORD*)ptr = 0 ; ptr++ ;ptr++ ;
                    // name opcu null string
                    *(DWORD*)ptr = 0xffffffff ; ptr++ ;ptr++ ;ptr++ ;ptr++ ;




    _internal_buf_size = ptr - debut;
    mylog(2,"OPCUACommand::CreateSessionRequest _internal_buf_size=%d...",_internal_buf_size);
    //*(DWORD*)ptr_len1 = (ptr-debut);
        *ptr_len1 = (int)_internal_buf_size%256; ptr_len1++ ;
        *ptr_len1 = (int)_internal_buf_size/256; ptr_len1++ ;
        *ptr_len1 = (int)_internal_buf_size/(256*256); ptr_len1++ ;
        *ptr_len1 = (int)_internal_buf_size/(256*256*256); ptr_len1++ ;    
    return _internal_buf_size ;
}