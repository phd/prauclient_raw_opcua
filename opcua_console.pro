#-------------------------------------------------
#
# Project created by QtCreator 2016-01-04T16:12:03
#
#-------------------------------------------------

QT       += core

QT       -= gui

LIBS += /usr/lib/x86_64-linux-gnu/libqjson.so

TARGET = opcua_console
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += \ 
    main.cpp link.cpp command.cpp create_session.cpp hello.cpp activate_session.cpp  read_request.cpp \

HEADERS += \
    main.h 
