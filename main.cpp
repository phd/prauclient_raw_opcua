#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <syslog.h>
#include <QDebug>
#include <QDir>
//#include <QJson>

#include <sys/ioctl.h>
#include <net/if.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>

//#include "/usr/include/qjson/qjson_export.h"
//#include "/usr/include/qjson/parser.h"
//#include "/usr/include/x86_64-linux-gnu/qt5/QtCore/qjsondocument.h"
#include "main.h"

static struct termios oldSettings;
static struct termios newSettings;
static int log_level = 4;

OPCUALink myOPCUAlink;

char getch(void){
  return getchar();
}

void sig_handler(int signo)
{
   syslog (LOG_INFO,"sig_handler %d",signo);
   if (signo == SIGINT){
      printf("received SIGINT\n");
      syslog (LOG_INFO,"SIGINT");
      exit(-1);
   }
   if (signo == SIGUSR1)
      printf("received SIGUSR1\n");
   if (signo == SIGKILL)
      printf("received SIGKILL\n");
   if (signo == SIGSTOP)
      printf("received SIGSTOP\n");
}

/*  The constructor gets a instance of the QT application and sets it to “app”
    The “run” slot is where your code will actually start execution.
    When you are through running your code you must call “quit” to stop the application.  This will tell the QT application to terminate.
    While the QT application is in the process of terminating it will execute the slot aboutToQuitApp().  This is a good place to do any clean-up work.*/
myMainClass::myMainClass(QObject *parent) :    QObject(parent)
{    // get the instance of the main application
    app = QCoreApplication::instance();
    // setup everything here
    // create any global objects
    // setup debug and warning mode
    _stopRunning = false;
    //QCoreApplication::instance()->installEventFilter(&_eventFilter);
    QObject::connect(this,SIGNAL(finished()), app, SLOT(quit()) ,  Qt::QueuedConnection );
    //QObject::connect(this,SIGNAL(signal_finished(int)),this->parent(),SLOT(slot_beforeExiting()) ,  Qt::QueuedConnection );
}

// x sec after the application starts this method will run
// all QT messaging is running at this point so threads, signals and slots
// will all work as expected.
void myMainClass::run()
{    // Add your main code here
    qDebug() << "MainClass.Run";
    // you must call quit when complete or the program will stay in the messaging loop
    //theopcualink.init();
    //theopcualink.startNetwork();usleep(ONE_SEC_USLEEP/2);
    /*theGroupOfAgent.readSettings();
    theGroupOfAgent.startNetwork();
    theGroupOfAgent.startRegistration();
    theGroupOfAgent.setopcuaLink(&theopcualink);*/

    forever    {
        char key = getch();
        qDebug()<< "key pressed:" << key;

        if ( key=='m'){
            //_myopcualink.startMonitor(IP_TOUCH_PHONE_NUMBER);
        }
        if ( key=='R'){
            //_myopcualink.startRecord(IP_TOUCH_PHONE_NUMBER,55555,55556);
        }

        if ( key=='r'){
           printf("...");
            //_myUserAgent.sipRegister();
        }
        if ( key=='d'){
            //_myUserAgent.sipInvite();
        }
        if ( key=='h'){
            //_myUserAgent.sipBye();
        }
        if ( key=='q' || key=='Q'){
            quit();
            return;
        }
        //emit KeyPressed(key);
    }
    while(!_stopRunning){
        qDebug() << "MainClass running";
        sleep(1);
    }
    quit();
}

// call this routine to quit the application
void myMainClass::quit()
{
    qDebug() << "MainClass::quit";
    // you can do some cleanup here then do emit finished to signal CoreApplication to quit
    /*theGroupOfAgent.writeSettings();
    theGroupOfAgent.stopRegistration();
    theGroupOfAgent.stopNetwork();
    theopcualink.stopNetwork();*/
    emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
//this is a good place to delete any objects that were created in the constructor and/or to stop any threads
void myMainClass::aboutToQuitApp()
{
    qDebug() << "aboutToQuitApp... stop threads ... sleep .... delete obj";
}

bool myCoreApp::event(QEvent *event)
{
    qDebug() << "CoreApp::event..";
    /*if (event->type() == QEvent::KeyPress) {
        qDebug() << "QEvent::KeyPress..";
        QKeyEvent *ke = static_cast<QKeyEvent *>(event);
        if (ke->key() == Qt::Key_Q)
        {
            qDebug() << "QEvent::KeyPress:"<<ke->key();
            qDebug("Quit?");
            //qApp->quit();
            return true;
        }
    }*/
    return QCoreApplication::event(event);
}

void myCoreApp::beforeExiting()
{
    qDebug() << "CoreApp::beforeExiting..";
    quit();
}

void mylog(unsigned char level,const char* fmt, ...)
{
   //return; //phd20161010
   if ( level > log_level ) return;
    static QMutex mut;
    if (mut.tryLock()){
        if ( 0==fmt ) return;
        if ( 0==fmt[0] ) return;
        if ( '\n'==fmt[0] ) return;

        struct timeval tv;
        gettimeofday (&tv, NULL);
        long milliseconds;
        milliseconds = tv.tv_usec / 1000;

        char time_string[40];
        struct tm* ptm = localtime (&tv.tv_sec);
        strftime (time_string, sizeof (time_string), "%Y-%m-%d %H:%M:%S", ptm);

        va_list args;
        va_start(args, fmt);
        //nb = vsnprintf(temp, sizeof(temp) , fmt, args);
        char temp[1024]={0};
        vsprintf(temp, fmt, args);
        va_end (args);

        // oter tous les '\n' en fin de chaine
        while ( temp!=NULL && temp[0] && temp[strlen(temp)-1] == '\n' ) temp[strlen(temp)-1] = 0;

        char horodate[128];
        time_t t = time(NULL);
        strftime(horodate, sizeof(horodate), "%Y-%m-%d", localtime(&t));
        char filename[128];
        //sprintf(filename,"/var/log/recorder/log-%s-PID-%d-%s.log",progname,getpid(),horodate);
        sprintf(filename,"./log/log-opcua-PID-%d-%s.log",getpid(),horodate);

        FILE* fout=fopen(filename,"a+t") ;
        if ( fout) {
            //strftime(horodate, sizeof(horodate), "%Y-%m-%d %H:%M:%S", localtime(&t));
            strftime(horodate, sizeof(horodate), "%H:%M:%S", localtime(&t));
            fprintf(fout,"%s.%03ld %s\n",horodate,milliseconds,temp);
            fclose(fout);
            fprintf(stderr,"%s.%03ld %s\n",horodate,milliseconds,temp);
        }else{
            fprintf(stderr,"log file error [%d]\n",filename);
        }
        mut.unlock();
    }
}

int main(int argc, char *argv[])
{
#ifdef WITH_GUI

   qDebug() << "main GUI starting...";
   QApplication app(argc, argv);
   myDialog myDlg;
   myDlg.show();
   QTimer::singleShot(1000, &myDlg, SLOT(run()));

#else

   qDebug() << "main CONSOLE starting...";
   /*A few things worth noting here.
     A instance of the MainClass class called myMain is created.
     A signal in that MainClass called "finished" actually quits the application.
     A signal from the app works with a slot in myMain called aboutToQuitApp
     A 10ms timer sends a signal to the slot run in the myMain class.  This bootstraps your code.
     The last line “return app.exec()” starts all of the QT messaging including the Slots and Signals system across various threads.
     By the time myMain gets the signal on the “run” Slot the QT application structure is up and running.*/
   //QCoreApplication app(argc, argv);
   myCoreApp app(argc, argv);
   myMainClass myMain;// create the main class

   // connect up the signals
   //QObject::connect(&myMain, SIGNAL(finished()),&app, SLOT(slot_beforeExiting()));
   QObject::connect(&app, SIGNAL(aboutToQuit()),&myMain, SLOT(aboutToQuitApp()));
   // This code will start the messaging engine in QT and in ... sec it will start the execution in the MainClass.run routine;
   qDebug() << "timer starting...";
   QTimer::singleShot(1000, &myMain, SLOT(run()));

#endif


   openlog("opcua_client", LOG_PID|LOG_CONS, LOG_USER);
   syslog (LOG_INFO, "argv[0]=[%s]",argv[0]);
   syslog (LOG_INFO, "starting opcua_client "__DATE__" "__TIME__);
   syslog (LOG_INFO, qPrintable(app.applicationName()) );
   syslog (LOG_INFO, qPrintable(app.applicationDirPath()));
   syslog (LOG_INFO, qPrintable(app.applicationFilePath().constData() ) );
   syslog (LOG_INFO, qPrintable(QString("PID:%1").arg(app.applicationPid()) ) );
   syslog (LOG_INFO, qPrintable(QDir::currentPath()));

   if (signal(SIGINT, sig_handler) == SIG_ERR)
      fprintf(stderr,"\ncan't catch SIGINT\n");
   if (signal(SIGUSR1, sig_handler) == SIG_ERR)
      printf("\ncan't catch SIGUSR1\n");
   if (signal(SIGKILL, sig_handler) == SIG_ERR)
      printf("\ncan't catch SIGKILL\n");
   if (signal(SIGSTOP, sig_handler) == SIG_ERR)
      printf("\ncan't catch SIGSTOP\n");

   if ( argc >= 2 ) {
      syslog (LOG_INFO, "chdir [%s]",argv[1]);
      syslog (LOG_INFO, qPrintable(QDir::currentPath()));
      chdir(argv[1]);
   }   

   /* if(1){

        QString file_path="./json.ini";
        QFile file_obj(file_path);
        if(!file_obj.open(QIODevice::ReadOnly)){
            qDebug()<<"Failed to open "<<file_path;
            exit(1);
        }

        QTextStream file_text(&file_obj);
        QString json_string;
        json_string = file_text.readAll();
        file_obj.close();
        QByteArray json_bytes = json_string.toLocal8Bit();

        auto json_doc=QJsonDocument::fromJSON(json_bytes);

        if(json_doc.isNull()){
            qDebug()<<"Failed to create JSON doc.";
            exit(2);
        }
        if(!json_doc.isObject()){
            qDebug()<<"JSON is not an object.";
            exit(3);
        }

        QJsonObject json_obj=json_doc.object();

        if(json_obj.isEmpty()){
            qDebug()<<"JSON object is empty.";
            exit(4);
        }

    }*/

   qDebug() << "main CONSOLE initialization done...";
   printf("Compiled with Qt Version %s\n", QT_VERSION_STR);   //exit(0);
   myOPCUAlink.init();
   myOPCUAlink.startNetwork();
   return app.exec();
}

unsigned char hex2bin(char c)
{
    char out_c=0;
    if      (c >= 'a' && c <= 'f')  out_c = c - 'a' + 10;
    else if (c >= 'A' && c <= 'F')  out_c = c - 'A' + 10;
    else if (c >= '0' && c <= '9')  out_c = c - '0';
    else {     fprintf(stderr,"hex2bin err %c\n",c);   exit(-1); }
    return (unsigned char)out_c;
}

void hexdump(unsigned char* buf, int len)
{
    char horodate[128];
    time_t t = time(NULL);
    //strftime(horodate, sizeof(horodate), "%Y-%m-%d", localtime(&t));
    strftime(horodate, sizeof(horodate), "%H:%M:%S", localtime(&t));

    char prev_char='a';
    char* str = (char*)malloc(3*len+1);
    char* str_ascii = (char*)malloc(3*len+1);
    if ( str ){
        int str_len=0;
        int str_len_ascii=0;
        for( int k=0,m=0; k< len; k++,m++){
            if (isalnum(buf[k]) ||  ispunct(buf[k]) )
                str_len_ascii += sprintf(str_ascii+str_len_ascii, " %c ", buf[k]);
            else
                str_len_ascii += sprintf(str_ascii+str_len_ascii, "...");

            str_len += sprintf(str+str_len, "%02X-", buf[k]);
            if ( (m && ((m % 64)==0)) || ( prev_char=='\r' && buf[k]=='\n' ) ){
                //fprintf(stderr,"%s:[%s]\n",horodate,str);
                fprintf(stderr,"%s:[%s]\n",horodate,str_ascii);
                mylog(2,"%s:%s",horodate,str_ascii);
                str_len=0;
                str_len_ascii=0;
                m=0;
            }
            prev_char = buf[k];
        }
        /*if (prefix!=NULL)
            fprintf(stderr,"%s[%s]",prefix, str);
        else
            fprintf(stderr,"[%s]",str);*/

        if (0){

            struct timeval tv;
            gettimeofday (&tv, NULL);
            long milliseconds;
            milliseconds = tv.tv_usec / 1000;

            char time_string[40];
            struct tm* ptm = localtime (&tv.tv_sec);
            strftime (time_string, sizeof (time_string), "%Y-%m-%d %H:%M:%S", ptm);

            char horodate[128];
            time_t t = time(NULL);
            strftime(horodate, sizeof(horodate), "%Y-%m-%d", localtime(&t));
            char filename[128];
            //sprintf(filename,"/var/log/recorder/log-%s-PID-%d-%s.log",progname,getpid(),horodate);
            sprintf(filename,"log-cstarec-PID-%d-%s.log",getpid(),horodate);

            FILE* fout=fopen(filename,"a+t") ;
            if ( fout) {
                //strftime(horodate, sizeof(horodate), "%Y-%m-%d %H:%M:%S", localtime(&t));
                strftime(horodate, sizeof(horodate), "%H:%M:%S", localtime(&t));
                fprintf(fout,"%s.%03ld[%s]\n",horodate,milliseconds,str);
                fclose(fout);
            }
        }
        free (str);
    }
}



