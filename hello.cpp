#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <syslog.h>
#include <QDebug>
#include <QDir>
#include <sys/ioctl.h>
#include <net/if.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "main.h"

int OPCUACommand::Hello(QString endpoint_url)
{
    mylog(5,"OPCUACommand::Hello...");
    //mylog(5,"ip1=%02X",ip1);    mylog(5,"ip2=%02X",ip2);    mylog(5,"ip3=%02X",ip3);    mylog(5,"ip4=%02X",ip4);

    BYTE* debut = _internal_buf;// + 2;
    BYTE* ptr = debut;
    
    *ptr++ = 0x48; // H
    *ptr++ = 0x45; // E
    *ptr++ = 0x4c; // L
    *ptr++ = 0x46; // F

    // message size 4 BYTES    
    BYTE* ptr_len1 = ptr; 
    *ptr = 0x11; ptr++ ;
    *ptr = 0x22; ptr++ ;
    *ptr = 0x33; ptr++ ;
    *ptr = 0x44; ptr++ ;

    // protocol version size 4 BYTES
    *ptr = 0; ptr++ ;
    *ptr = 0; ptr++ ;
    *ptr = 0; ptr++ ;
    *ptr = 0; ptr++ ;

    // ReceiveBufferSize = 65536; 4 BYTES
    *(DWORD*)ptr = 65536; ptr++ ;ptr++ ;ptr++ ;ptr++ ;
    // SendBufferSize = 65536; 4 BYTES
    *(DWORD*)ptr = 65536; ptr++ ;ptr++ ;ptr++ ;ptr++ ;
    // MaxMessageSize = 65536; 4 BYTES
    *ptr = 0; ptr++ ;
    *ptr = 0; ptr++ ;
    *ptr = 0; ptr++ ;
    *ptr = 0; ptr++ ;

    // MaxChunkCount  4 BYTES
    *ptr = 0; ptr++ ;
    *ptr = 0; ptr++ ;
    *ptr = 0; ptr++ ;
    *ptr = 0; ptr++ ;

    // endpoint URL
        // length
        //*(DWORD*)ptr = endpoint_url.length();  ptr++ ;ptr++ ;ptr++ ;ptr++ ;
        *ptr = endpoint_url.length(); ptr++ ;
        *ptr = 0; ptr++ ;
        *ptr = 0; ptr++ ;
        *ptr = 0; ptr++ ;
        
        // string data
        for (int i=0; i<endpoint_url.length() ; i++){
            *ptr++ = endpoint_url[i].toAscii();
            //*ptr++ = '1'; *ptr++ = '9';*ptr++ = '3';*ptr++ = '0' ;  // ascii string
            //*ptr++ = digit1; *ptr++ = digit2;*ptr++ = digit3;*ptr++ = digit4;  // ascii string
        }    
    
    _internal_buf_size = ptr - debut;
    //ptr--;*ptr_len1 = (ptr-ptr_len1);
    //*(DWORD*)ptr_len1 = (ptr-debut);
        *ptr_len1 = (int)_internal_buf_size%256; ptr_len1++ ;
        *ptr_len1 = (int)_internal_buf_size/256; ptr_len1++ ;
        *ptr_len1 = (int)_internal_buf_size/(256*256); ptr_len1++ ;
        *ptr_len1 = (int)_internal_buf_size/(256*256*256); ptr_len1++ ;        
    return _internal_buf_size ;
}

